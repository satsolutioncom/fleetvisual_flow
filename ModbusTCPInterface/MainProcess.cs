﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ModbusTCPInterface.CrossCutting;
using System.Threading;
using ModbusTCPInterface.Services;
using ModbusTCPInterface.Repository;
using System.Globalization;

namespace ModbusTCPInterface
{
    public class MainProcess
    {
        private Timer timer;
        private ModbusReceiver modbusReceiver;
        private ResourceService resourceService;
        private ModbusService modbusService;
        int? interval;


        public void Start(string[] args)
        {
            if (args.ElementAtOrDefault(0) != null)
            {
                interval = Convert.ToInt32(args[0]);
            }

            Init();
            Main();
            Console.WriteLine("Started");
            LogHelper.Ins.Write("Service Started", LogLevel.Information);
            Console.ReadLine();
        }

        public void Stop()
        {
            LogHelper.Ins.Write("Service Stop", LogLevel.Information);
        }


        public void Init()
        {
            LogHelper.Ins.RegisterLog(new FileLogger(AppDomain.CurrentDomain.BaseDirectory));
            try
            {
                ConfigurationManager.Ins.Start();

                if (interval == null)
                    interval = Convert.ToInt32(ConfigurationManager.Ins.Config.General.TimeScan) * 1000;

   

                string pathDatalogger = ConfigurationManager.Ins.Config.General.PathDataLogger;
                string formatFile = ConfigurationManager.Ins.Config.General.FormatFile;
                string prefix = ConfigurationManager.Ins.Config.General.PrefixFile;
                resourceService = new ResourceService(pathDatalogger, formatFile, prefix);
            }
            catch (Exception ex)
            {
                LogHelper.Ins.Write(ex.ToString(), LogLevel.Error);
                Console.WriteLine(ex.ToString());
            }
        }

        public void Main()
        {
            try
            {
                var autoEvent = new AutoResetEvent(false);
                timer = new Timer(HandleTimer,autoEvent,2000,(int)interval);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private  void HandleTimer(Object stateInfo)
        {
            try
            {
                string IpMb = ConfigurationManager.Ins.Config.Source.IPAddress;
                ushort port = ConfigurationManager.Ins.Config.Source.Port;
                int startAddress = ConfigurationManager.Ins.Config.Source.StartAddress;

                modbusService = new ModbusService(IpMb, port, startAddress);

                var formatTime = ConfigurationManager.Ins.Config.Source.FormatDatetime;
                var timestamp = DateTime.UtcNow.ToString(formatTime, CultureInfo.CreateSpecificCulture("en-US"));
                List<DataInfo> data = new List<DataInfo>();
                var channels = ConfigurationManager.Ins.Config.Channels;
                foreach (var channel in channels)
                {
                    List<DataInfo> d1 = new List<DataInfo>();
                    var orders = modbusService.GetTagOrder(channel);
                    foreach (var order in orders)
                    {
                        var tags = channel.Tags.Where(x => x.Order == order).ToList();
                        var val = modbusService.GetData(tags, channel.Id, timestamp);
                        if (val != null && val.Count() > 0)
                            d1.AddRange(val);
                    }

                    if (d1.Count() == 0)
                    {
                        int i = 0;
                        bool check = true;
                        while(i<3 || check)
                        {
                            var timeout = ConfigurationManager.Ins.Config.Source.Timeout;
                            Thread.Sleep(timeout*1000);
                            var orders1 = modbusService.GetTagOrder(channel);
                            foreach (var order in orders1)
                            {
                                var tags = channel.Tags.Where(x => x.Order == order).ToList();
                                var val = modbusService.GetData(tags, channel.Id, timestamp);
                                if (val != null && val.Count() > 0)
                                {
                                    d1.AddRange(val);

                                } 
                            }
                            if (d1.Count > 0)
                            {
                                check = false;
                            }
                            i++;
                        }
                        if(d1.Count() == 0)
                        {
                            LogHelper.Ins.Write("ID : " + channel.Id + "  not found values", LogLevel.Warning);
                        }
           
                    }
                    if(d1.Count > 0)
                    {
                        data.AddRange(d1);
                    }
                }
                if(data.Count>0)
                    resourceService.WriteDataLogger(data);
                modbusService.disconnect();
                GC.Collect();
            }
            catch (Exception ex)
            {
                LogHelper.Ins.Write(ex.ToString(), LogLevel.Error);
                Console.WriteLine(ex.ToString());
            }
        }

        public void TryThreeTimes(Action action)
        {
            var tries = 3;
            while (true)
            {
                try
                {
                    action();
                    break; // success!
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
