﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModbusTCPInterface.Repository
{
    public class ConfigJson
    {
        private General general;
        private Source source;
        private List<Channel> channels = new List<Channel>();

        public General General
        {
            get => general;
            set => general = value;
        }
        public Source Source
        {
            get => source;
            set => source = value;
        }
        public List<Channel> Channels
        {
            get => channels;
            set => channels = value;
        }
    }

    public class General
    {
        private int timeScan;
        private string pathDataLogger;
        private string formatFile;
        private string prefixFile;

        public int TimeScan
        {
            get
            {
                if (timeScan == 0)
                {
                    throw new ArgumentNullException("Time scan config failed", "Time scan config not set");
                }
                return timeScan;
            }
            set => timeScan = value;
        }
        public string PathDataLogger
        {
            get
            {
                if (pathDataLogger== null)
                {
                    throw new ArgumentNullException("Path Data Logger config failed", "Path Data Logger config not set");
                }
                return pathDataLogger;
            }
            set => pathDataLogger = value;
        }
        public string FormatFile
        {
            get
            {
                if (formatFile == null)
                {
                    throw new ArgumentNullException("Format file config failed", "Format file config not set");
                }
                return formatFile;
            }
            set => formatFile = value;
        }

        public string PrefixFile
        {
            get => prefixFile;
            set => prefixFile = value;
        }
    }

    public class Source
    {
        private string ipAddress;
        public string IPAddress
        {
            get
            {
                if (ipAddress == null)
                {
                    throw new ArgumentNullException("IP address config failed", "IP address config not set");
                }
                return ipAddress;
            }
            set => ipAddress = value;
        }

        private ushort port;
        public ushort Port
        {
            get
            {
                if (port == 0)
                {
                    throw new ArgumentNullException("Port config failed", "Port config not set");
                }
                return port;
            }
            set => port = value;
        }

        private int startAddress;
        public int StartAddress
        {
            get => startAddress;
            set => startAddress = value;
        }

        private string formatDatetime;
        public string FormatDatetime
        {
            get => formatDatetime;
            set => formatDatetime = value;
        }

        private int timeout;
        public int Timeout { get; set; }
    } 

    public class Channel
    {
        public byte Id { get; set; }
        public List<Tag>  Tags { get; set; }
    }

    public class Tag
    {
        public string TagName { get; set; }
        public UInt32 Address { get; set; }
        public string DataType { get; set; }
        public int Order { get; set; }
        public List<int> BytesIndex { get; set; }
    }
    
}
