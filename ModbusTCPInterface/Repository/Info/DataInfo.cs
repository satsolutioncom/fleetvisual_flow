﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModbusTCPInterface.Repository
{
    public class DataInfo
    {
        public string TagName { get; set; }
        public string Timestamp { get; set; }
        public string Value { get; set; }
        public string Quality { get; set; }
    }
}
