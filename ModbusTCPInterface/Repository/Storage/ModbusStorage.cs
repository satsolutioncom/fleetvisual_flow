﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModbusTCPInterface.Repository
{
    public class ModbusStorage
    {
        private static readonly Lazy<ModbusStorage> lazy =
new Lazy<ModbusStorage>(() => new ModbusStorage());
        public static ModbusStorage Ins { get { return lazy.Value; } }
        private ModbusStorage()
        {
       
        }

        public List<DataInfo> data = new List<DataInfo>();

    }
}
