﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModbusTCPInterface
{
    class Program
    {
        private static Thread thread;
        private static MainProcess main;
        public class Service : ServiceBase
        {
            public Service() { }

            protected override void OnStart(string[] args)
            {
                Program.Start(args);
            }

            protected override void OnStop()
            {
                Program.Stop();
            }
        }

        static void Main(string[] args)
        {
            ResetRootDirectory();
            if (!Environment.UserInteractive)
            {
                using (var service = new Service())
                    ServiceBase.Run(service);
            }
            else
            {
                Start(args);
                Stop();
            }
        }

        private static void Start(string[] args)
        {
            main = new MainProcess();
            thread = new Thread(new ThreadStart(() => ThreadStart(args)));
            thread.Start();
        }

        private static void Stop()
        {
            main.Stop();
            GC.Collect();
        }

        private static void ThreadStart(string[] args)
        {
            try
            {
                main.Start(args);
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }

        private static void ResetRootDirectory()
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
