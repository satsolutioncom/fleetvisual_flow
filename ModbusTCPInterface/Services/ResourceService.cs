﻿using ModbusTCPInterface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModbusTCPInterface.CrossCutting;

namespace ModbusTCPInterface.Services
{
    public class ResourceService
    {
        private string PathDataLogger;
        private string FormatFile;
        private string PrefixFile;

        public ResourceService(string path, string formatFile, string prefix)
        {
            PathDataLogger = path;
            FormatFile = formatFile;
            PrefixFile = prefix;
        }

        public void WriteDataLogger(List<DataInfo> data)
        {
            StringBuilder str = new StringBuilder();
            foreach(var d in data)
            {
                str.Append($"{d.TagName},{d.Timestamp},{d.Value},{d.Quality}" + Environment.NewLine);
            }

            var res = str.ToString().Trim();
            if(res != string.Empty)
            ResourceManger.WriteContent(PathDataLogger, res, FormatFile, PrefixFile);
        }
    }
}
