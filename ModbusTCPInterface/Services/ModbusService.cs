﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModbusTCPInterface.ThirdParty;
using ModbusTCPInterface.Repository;
using ModbusTCPInterface.CrossCutting;

namespace ModbusTCPInterface.Services
{
    public class ModbusService
    {
        private ModbusTcp modbusTcp;
        public string Ip { get; set; }
        public ushort Port { get; set; }
        private int StartAddress;
        public ModbusService(string ip ,ushort port,int startAddress)
        {
            Ip = ip;
            Port = port;
            StartAddress = startAddress;
            InitialMB();
        }

        private void InitialMB()
        {
            while (modbusTcp == null)
            {
                try
                {
                    modbusTcp = new ModbusTcp(Ip, Port);
                }
                catch (Exception ex)
                {
                    string err = $"Error connection failed : Ip {Ip}  port {Port}  \r\n {ex.ToString()} ";
                    LogHelper.Ins.Write(err, LogLevel.Error);
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        public void Connect()
        {
            if (!modbusTcp.connected)
            {
                try
                {
                    modbusTcp.connect(Ip, Port);
                    LogHelper.Ins.Write("Connectd " + Ip + ":" + Port, LogLevel.Information);
                }
                catch (Exception ex)
                {
                    LogHelper.Ins.Write("Connection failed.",LogLevel.Error);
                }

            }
        }

        public void disconnect()
        {
            if (modbusTcp.connected)
            {
                try
                {
                    modbusTcp.disconnect();
                    modbusTcp.Dispose();
                }
                catch (Exception ex)
                {
                    LogHelper.Ins.Write("disconnect failed.", LogLevel.Error);
                }
            }
        }

        public byte[] ReadInputRegister(ushort Id,byte unit, ushort startAddress, byte size)
        {
            try
            {
                Connect();
                var bytes = new byte[1];
                modbusTcp.ReadInputRegister(Id, unit, startAddress, size, ref bytes);
                return bytes;
            }
            catch(Exception ex)
            {
                return null;
            }
        }





       

        private void SwapBytes(byte[] bytes,int startIndex,List<int> bytesIndex,int size)
        {
            var newbytes = new byte[size];
            int count = 0;
            foreach(var i in bytesIndex)
            {
                newbytes[count] = bytes[startIndex + i];
                count++;
            }
            for(int j = 0; j < newbytes.Length; j++)
            {
                bytes[startIndex + j] = newbytes[j];
            }
        }

        private string ConvertValue(byte[] bytes, int startIndex, string dataType)
        {
            if (dataType == "float")
                return BitConverter.ToSingle(bytes, startIndex).ToString();
            else if (dataType == "byte")
                return bytes[startIndex].ToString();
            else if (dataType == "double")
                return BitConverter.ToDouble(bytes, startIndex).ToString();
            else if (dataType == "long")
                return BitConverter.ToInt64(bytes, startIndex).ToString();
            else if (dataType == "uint16")
                return BitConverter.ToUInt16(bytes, startIndex).ToString();
            else if (dataType == "uint32")
                return BitConverter.ToUInt32(bytes, startIndex).ToString();
            else if (dataType == "string")
                return System.Text.Encoding.UTF8.GetString(bytes);
            return null;
        }

        private byte[] ConvertIndexByte(byte[] bytes)
        {
            var newbytes = new byte[bytes.Length];
            for(int i = 0; i < bytes.Length; i+=2)
            {
                newbytes[i] = bytes[i + 1];
                newbytes[i+1] = bytes[i];
            }
            return newbytes;
        }

       public IEnumerable<int> GetTagOrder(Channel channel)
        {
            var order = channel.Tags
                .GroupBy(c => c.Order, (key, c) => c.FirstOrDefault())
                .Select(x => x.Order);
            return order;
        }

        public List<DataInfo> GetData(List<Tag> tags,byte unit,string timestamp)
        {
            //เอา address ต่ำสุด
            var startAddress = GetStartAddress(tags);
            // ตัด 3 ตัวหน้าออก
            var strStartAddress = GetAddress(startAddress.Address);
            // convert เป็น ushort
            ushort numStrAddress = ReadStartAdr(strStartAddress);
            if (StartAddress == 0)
                numStrAddress -= 1;
            var dataTypes = tags.Select(x => x.DataType).ToList();
            // get byte ที่จะอ่าน / 2
            byte size = Convert.ToByte(GetSize(dataTypes));
            ushort Id = 4;
            var bytes = ReadInputRegister(Id, unit, numStrAddress, size);

            if (bytes == null)
            {
                List<DataInfo> badData = new List<DataInfo>();
                badData = CreateBadData(tags, timestamp);
                return badData;
            }

            var data = ConvertBytesToValues(bytes, tags, timestamp);
            return data;
        }

        private List<DataInfo> CreateBadData(List<Tag> tags, string timestamp)
        {
            List<DataInfo> datas = new List<DataInfo>();
            foreach (var tag in tags.OrderBy(x => x.Address))
            {
                datas.Add(new DataInfo()
                {
                    TagName = tag.TagName,
                    Timestamp = timestamp,
                    Value = "999999",
                    Quality = "0"
                });

            }

            return datas;
        }


        public List<DataInfo> ConvertBytesToValues(byte[] values, List<Tag> tags, string timestamp)
        {
            List<DataInfo> datas = new List<DataInfo>();
            //  var newbytes = ConvertIndexByte(values);

            if (values.Length < 2) return null;

            int index = 0;
            foreach (var tag in tags.OrderBy(x => x.Address))
            {
                var size = GetSizeData(tag.DataType);
                SwapBytes(values, index, tag.BytesIndex, size);
                var val = ConvertValue(values, index, tag.DataType);
                index += size;
                datas.Add(new DataInfo()
                {
                    TagName = tag.TagName,
                    Timestamp = timestamp,
                    Value = val.ToString(),
                    Quality = "1"
                });
            }
            return datas;
        }

        public Tag GetStartAddress(List<Tag> tag)
        {
            var startAdd = tag.OrderBy(x => x.Address).FirstOrDefault();
            return startAdd;
        }

        public string GetAddress(uint startAdd)
        {
            string strAdd = startAdd.ToString().Substring(1, startAdd.ToString().Length - 1);
            return strAdd;
        }

        public ushort ReadStartAdr(string address)
        {
            if (address.IndexOf("0x", 0, address.Length) == 0)
            {
                string str = address.Replace("0x", "");
                ushort hex = Convert.ToUInt16(str, 16);
                return hex;
            }
            else
            {
                return Convert.ToUInt16(address);
            }
        }

        public int GetSize(List<string> str)
        {
            int size = 0;
            foreach(var s in str)
            {
                var si = GetSizeData(s) / 2;
                if (si < 1)
                    si = 1;
                size += si;
            }
            return size;
        }

        private int GetSizeData(string _type)
        {
            _type = _type.ToLower();
            int size = 0;
            switch (_type)
            {
                case "byte":
                    size = 1;
                    break;
                case "float":
                    size = 4;
                    break;
                case "double":
                    size = 8;
                    break;
                case "long":
                    size = 8;
                    break;
                case "uint16":
                    size = 2;
                    break;
                case "uint32":
                    size = 4;
                    break;
                case "string":
                    size = 8;
                    break;
            }
            return size;
        }
    }
}
