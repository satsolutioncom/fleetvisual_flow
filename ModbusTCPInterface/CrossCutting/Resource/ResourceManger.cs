﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModbusTCPInterface.CrossCutting
{
    public class ResourceManger
    {
        public static string ReadFile(string fullPath)
        {
            //var content = File.ReadAllText(fullPath);

            var outStream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            string content;
            using (StreamReader reader = new StreamReader(outStream))
            {
                content = reader.ReadToEnd();
                reader.Close();
                outStream.Close();
            }
            
            return content;
        }

        public static string GetDirectory(string fullpath)
        {
            var dir = Path.GetDirectoryName(fullpath);
            return dir;
        }

        private static void CheckPath(string fullpath)
        {
            var dir = GetDirectory(fullpath);
            CreateDirectories(dir);
            CreateFile(fullpath);
        }


        public static void CreateDirectories(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void CreateFile(string path)
        {
            if (!File.Exists(path))
            {
                var outStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                outStream.Close();
            }

        }

        public static void WriteContent(string path, string content, string formatFile, string prefix)
        {
            string _pre = (prefix == null) ? "" : prefix + "_";
            string fullpath = path + "\\" + _pre + GetFileName(formatFile);
            CheckPath(fullpath);
            using (FileStream aFile = new FileStream(fullpath, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(aFile))
            {
                sw.WriteLine(content);
                sw.Close();
                aFile.Close();
            }
        }


        private static string GetFileName(string formatFile)
        {
            var datetime = DateTime.UtcNow.ToString(formatFile, CultureInfo.CreateSpecificCulture("en-US"));
            return datetime + ".txt";
        }
    }
}
