﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModbusTCPInterface.Repository;
using Newtonsoft.Json;

namespace ModbusTCPInterface.CrossCutting
{
    public class ConfigurationManager
    {
        private static readonly Lazy<ConfigurationManager> lazy =
new Lazy<ConfigurationManager>(() => new ConfigurationManager());

        public static ConfigurationManager Ins { get { return lazy.Value; } }

        public ConfigJson Config;

        private ConfigurationManager()
        {
            Config = new ConfigJson();
        }
             
        public void Start()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "config.json";
            string strconfig = ResourceManger.ReadFile(path);
            Config = JsonConvert.DeserializeObject<ConfigJson>(strconfig);
        }
    }
}
